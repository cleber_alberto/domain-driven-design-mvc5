﻿using DomainDrivenDesign.Application.Applications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainDrivenDesign.Application.ViewModels;

namespace DomainDrivenDesign.Mvc.Controllers
{
    public class CursoController : Controller
    {
        private readonly CursoApplicationService _cursoApplicationService;

        public CursoController(CursoApplicationService cursoApplicationService)
        {
            _cursoApplicationService = cursoApplicationService;
        }

        // GET: Curso
        public ActionResult Index()
        {
            var model = _cursoApplicationService.Listar();
            return View(model);
        }

        // GET: Curso/Details/5
        public ActionResult Details(Guid id)
        {
            var model = _cursoApplicationService.Obter(id);
            return View(model);
        }

        // GET: Curso/Create
        public ActionResult Create()
        {
            var model = new CursoViewModel();
            return View(model);
        }

        // POST: Curso/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            var model = new CursoViewModel();

            if (!TryUpdateModel(model))
                ModelState.AddModelError(string.Empty, "Ocorreu um erro ao tentar validar o formulário.");

            if (ModelState.IsValid)
            {
                try
                {
                    _cursoApplicationService.AdicionarOuModificar(model);
                    return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }

            return View(model);
        }

        // GET: Curso/Edit/5
        public ActionResult Edit(Guid id)
        {
            var model = _cursoApplicationService.Obter(id);
            return View(model);
        }

        // POST: Curso/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, FormCollection collection)
        {
            var model = _cursoApplicationService.Obter(id);

            if (!TryUpdateModel(model))
                ModelState.AddModelError(string.Empty, "Ocorreu um erro ao tentar validar o formulário.");

            if (ModelState.IsValid)
            {
                try
                {
                    _cursoApplicationService.AdicionarOuModificar(model);
                    return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }

            return View(model);
        }

        // GET: Curso/Delete/5
        public ActionResult Delete(Guid id)
        {
            var model = _cursoApplicationService.Obter(id);
            return View(model);
        }

        // POST: Curso/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, FormCollection collection)
        {
            var model = _cursoApplicationService.Obter(id);

            try
            {
                _cursoApplicationService.Remover(model);
                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                ModelState.AddModelError(string.Empty, exception.Message);
            }

            return View(model);
        }
    }
}
