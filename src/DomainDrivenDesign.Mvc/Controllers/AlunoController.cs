﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainDrivenDesign.Application.Interfaces;
using DomainDrivenDesign.Application.ViewModels;

namespace DomainDrivenDesign.Mvc.Controllers
{
    public class AlunoController : Controller
    {
        private readonly IAlunoApplicationService _alunoApplicationService;
        private readonly ICursoApplicationService _cursoApplicationService;

        public AlunoController(IAlunoApplicationService alunoApplicationService, ICursoApplicationService cursoApplicationService)
        {
            _alunoApplicationService = alunoApplicationService;
            _cursoApplicationService = cursoApplicationService;
        }

        // GET: Aluno
        public ActionResult Index()
        {
            var model = _alunoApplicationService.Listar();
            return View(model);
        }

        // GET: Aluno/Details/5
        public ActionResult Details(Guid id)
        {
            var model = _alunoApplicationService.Obter(id);
            return View(model);
        }

        // GET: Aluno/Create
        public ActionResult Create()
        {
            var model = new AlunoViewModel();

            ObterCursos(model);
            return View(model);
        }

        // POST: Aluno/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            var model = new AlunoViewModel();
            ObterCursos(model);

            if (!TryUpdateModel(model))
                ModelState.AddModelError(string.Empty, "Ocorreu um erro ao tentar validar o formulário.");

            if (ModelState.IsValid)
            {
                try
                {
                    if (collection["curso.Selecionado"] != null)
                    {
                        var keys = Request.Form["curso.Selecionado"].Split(',');
                        foreach (var curso in model.Cursos)
                            curso.Selecionado = keys.Any(k => k == curso.Id.ToString());
                    }

                    _alunoApplicationService.AdicionarOuModificar(model);
                    return RedirectToAction("Index");
                }
                catch(Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }
            
            return View(model);
        }

        // GET: Aluno/Edit/5
        public ActionResult Edit(Guid id)
        {
            var model = _alunoApplicationService.Obter(id);

            ObterCursos(model);
            return View(model);
        }

        // POST: Aluno/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, FormCollection collection)
        {
            var model = _alunoApplicationService.Obter(id);
            ObterCursos(model);

            if (!TryUpdateModel(model))
                ModelState.AddModelError(string.Empty, "Ocorreu um erro ao tentar validar o formulário.");

            if (ModelState.IsValid)
            {
                try
                {
                    if (collection["curso.Selecionado"] != null)
                    {
                        var keys = Request.Form["curso.Selecionado"].Split(',');
                        foreach (var curso in model.Cursos)
                            curso.Selecionado = keys.Any(k => k == curso.Id.ToString());
                    }

                    _alunoApplicationService.AdicionarOuModificar(model);
                    return RedirectToAction("Index");
                }
                catch(Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }

            return View(model);
        }

        // GET: Aluno/Delete/5
        public ActionResult Delete(Guid id)
        {
            var model = _alunoApplicationService.Obter(id);
            return View(model);
        }

        // POST: Aluno/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, FormCollection collection)
        {
            try
            {
                _alunoApplicationService.Remover(id);
                return RedirectToAction("Index");
            }
            catch(Exception exception)
            {
                ModelState.AddModelError(string.Empty, exception.Message);
            }

            var model = _alunoApplicationService.Obter(id);
            return View(model);
        }

        private void ObterCursos(AlunoViewModel model)
        {
            var cursos = _cursoApplicationService.Listar();

            foreach (var curso in cursos)
            {
                var cursoModel = model.Cursos.FirstOrDefault(p => p.Id == curso.Id);

                if (cursoModel == null)
                    model.Cursos.Add(new CursoViewModel {Id = curso.Id, Descricao = curso.Descricao});
                else
                    cursoModel.Selecionado = true;
            }
        }
    }
}
