﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainDrivenDesign.Application.Interfaces;
using DomainDrivenDesign.Application.ViewModels;

namespace DomainDrivenDesign.Mvc.Controllers
{
    public class ProfessorController : Controller
    {
        private readonly IProfessorApplicationService _professorApplicationService;
        private readonly ICursoApplicationService _cursoApplicationService;

        public ProfessorController(IProfessorApplicationService professorApplicationService, ICursoApplicationService cursoApplicationService)
        {
            _professorApplicationService = professorApplicationService;
            _cursoApplicationService = cursoApplicationService;
        }

        // GET: Professor
        public ActionResult Index()
        {
            var model = _professorApplicationService.Listar();
            return View(model);
        }

        // GET: Professor/Details/5
        public ActionResult Details(Guid id)
        {
            var model = _professorApplicationService.Obter(id);
            return View(model);
        }

        // GET: Professor/Create
        public ActionResult Create()
        {
            var model = new ProfessorViewModel();
            ObterCursos(model);
            return View(model);
        }

        // POST: Professor/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            var model = new ProfessorViewModel();

            if(!TryUpdateModel(model))
                ModelState.AddModelError(string.Empty, "Ocorreu um erro ao tentar validar o formulário.");

            if (ModelState.IsValid)
            {
                try
                {
                    _professorApplicationService.AdicionarOuModificar(model);
                    return RedirectToAction("Index");
                }
                catch(Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }

            ObterCursos(model);
            return View(model);
        }

        // GET: Professor/Edit/5
        public ActionResult Edit(Guid id)
        {
            var model = _professorApplicationService.Obter(id);
            ObterCursos(model);
            return View(model);
        }

        // POST: Professor/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, FormCollection collection)
        {
            var model = _professorApplicationService.Obter(id);

            if(!TryUpdateModel(model))
                ModelState.AddModelError(string.Empty, "Ocorreu um erro ao tentar validar o formulário.");

            if (ModelState.IsValid)
            {
                try
                {
                    _professorApplicationService.AdicionarOuModificar(model);
                    return RedirectToAction("Index");
                }
                catch(Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }

            ObterCursos(model);
            return View(model);
        }

        // GET: Professor/Delete/5
        public ActionResult Delete(Guid id)
        {
            var model = _professorApplicationService.Obter(id);
            return View(model);
        }

        // POST: Professor/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, FormCollection collection)
        {
            try
            {
                _professorApplicationService.Remover(id);
                return RedirectToAction("Index");
            }
            catch(Exception exception)
            {
                ModelState.AddModelError(string.Empty, exception.Message);
            }

            var model = _professorApplicationService.Obter(id);
            return View(model);
        }

        private void ObterCursos(ProfessorViewModel model)
        {
            ViewBag.Cursos = new SelectList(_cursoApplicationService.Listar(), "Id", "Descricao", model.CursoId);
        }
    }
}
