﻿using System;
using System.Collections.Generic;
using DomainDrivenDesign.Application.Interfaces;
using DomainDrivenDesign.Application.Mapping;
using DomainDrivenDesign.Application.ViewModels;
using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Domain.Interfaces.Services;

namespace DomainDrivenDesign.Application.Applications
{
    public class ProfessorApplicationService : ApplicationServiceBase, IProfessorApplicationService
    {
        private readonly IUnitOfWork _uow;
        private readonly IProfessorRepository _professorRepository;
        private readonly IProfessorService _professorService;
        private readonly ICursoRepository _cursoRepository;

        public ProfessorApplicationService(IUnitOfWork uow, IProfessorRepository professorRepository, IProfessorService professorService, ICursoRepository cursoRepository)
        {
            _uow = uow;
            _professorRepository = professorRepository;
            _professorService = professorService;
            _cursoRepository = cursoRepository;
        }

        public IEnumerable<ProfessorViewModel> Listar()
        {
            return Mapper<Professor, ProfessorViewModel>.AsEnumerable(_professorRepository.Obter(p => p.Ativo, "Curso"));
        }

        public ProfessorViewModel Obter(Guid id)
        {
            return Mapper<Professor, ProfessorViewModel>.Single(_professorRepository.Encontrar(p => p.Id == id, "Curso"));
        }

        public void AdicionarOuModificar(ProfessorViewModel model)
        {

            var curso = _cursoRepository.ObterPorId(model.CursoId);

            if(curso == null)
                throw new NullReferenceException("Curso não localizado.");

            var professor = _professorRepository.ObterPorId(model.Id);

            if (professor == null)
            {
                professor = Professor.Create(model.Nome, model.Sobrenome, curso);
                _professorService.Adicionar(professor);
            }
            else
            {
                professor.DefinirNome(model.Nome, model.Sobrenome);
                professor.DefinirCurso(curso);
                _professorService.Modificar(professor);
            }

            _uow.Commit();
        }

        public void Remover(Guid id)
        {
            var professor = _professorRepository.ObterPorId(id);

            if (professor == null)
                throw new NullReferenceException("Professor não localizado.");

            professor.Inativar();
            _professorService.Modificar(professor);
            _uow.Commit();
        }
    }
}