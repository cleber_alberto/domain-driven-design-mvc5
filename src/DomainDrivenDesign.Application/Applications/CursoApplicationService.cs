﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainDrivenDesign.Application.Interfaces;
using DomainDrivenDesign.Application.Mapping;
using DomainDrivenDesign.Application.ViewModels;
using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Domain.Interfaces.Services;

namespace DomainDrivenDesign.Application.Applications
{
    public class CursoApplicationService : ApplicationServiceBase, ICursoApplicationService
    {
        private readonly IUnitOfWork _uow;
        private readonly ICursoRepository _cursoRepository;
        private readonly ICursoService _cursoService;

        public CursoApplicationService(IUnitOfWork uow, ICursoRepository cursoRepository, ICursoService cursoService)
        {
            _uow = uow;
            _cursoRepository = cursoRepository;
            _cursoService = cursoService;
        }

        public IEnumerable<CursoViewModel> Listar()
        {
            return Mapper<Curso, CursoViewModel>.AsEnumerable(_cursoRepository.Obter(p => p.Ativo));
        }

        public CursoViewModel Obter(Guid id)
        {
            return Mapper<Curso, CursoViewModel>.Single(_cursoRepository.ObterPorId(id));
        }

        public void AdicionarOuModificar(CursoViewModel model)
        {
            using (_uow)
            {
                var curso = _cursoRepository.ObterPorId(model.Id);

                if (curso == null)
                {
                    curso = Curso.Create(model.Descricao);
                    _cursoService.Adicionar(curso);
                }
                else
                {
                    curso.DefinirDescricao(model.Descricao);
                    _cursoService.Modificar(curso);
                }

                _uow.Commit();
            }
        }

        public void Remover(CursoViewModel model)
        {
            var curso = _cursoRepository.ObterPorId(model.Id);

            if(curso == null)
                throw new NullReferenceException("Curso não localizado.");

            curso.Inativar();

            _cursoService.Modificar(curso);
            _uow.Commit();
        }
    }
}