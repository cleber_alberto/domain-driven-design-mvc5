﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainDrivenDesign.Application.Interfaces;
using DomainDrivenDesign.Application.Mapping;
using DomainDrivenDesign.Application.ViewModels;
using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Domain.Interfaces.Services;

namespace DomainDrivenDesign.Application.Applications
{
    public class AlunoApplicationService : ApplicationServiceBase, IAlunoApplicationService
    {
        private readonly IUnitOfWork _uow;
        private readonly IAlunoService _alunoService;
        private readonly IAlunoRepository _alunoRepository;
        private readonly ICursoRepository _cursoRepository;

        public AlunoApplicationService(IUnitOfWork uow, IAlunoService alunoService, IAlunoRepository alunoRepository, ICursoRepository cursoRepository)
        {
            _uow = uow;
            _alunoService = alunoService;
            _alunoRepository = alunoRepository;
            _cursoRepository = cursoRepository;
        }

        public IEnumerable<AlunoViewModel> Listar()
        {
            return Mapper<Aluno, AlunoViewModel>.AsEnumerable(_alunoRepository.Obter(p => p.Ativo, "Cursos"));
        }

        public AlunoViewModel Obter(Guid id)
        {
            return Mapper<Aluno, AlunoViewModel>.Single(_alunoRepository.Encontrar(p => p.Id == id, "Cursos"));
        }

        public void AdicionarOuModificar(AlunoViewModel model)
        {
            var aluno = _alunoRepository.ObterPorId(model.Id);

            if (aluno == null)
            {
                aluno = Aluno.Create(model.Nome, model.Sobrenome, model.DataDeNascimento);
                Cursos(aluno, model.Cursos);

                _alunoService.Adicionar(aluno);
            }
            else
            {
                aluno.DefinirNome(model.Nome, model.Sobrenome);
                aluno.DefinirDataDeNascimento(model.DataDeNascimento);
                Cursos(aluno, model.Cursos);

                _alunoService.Modificar(aluno);
            }

            _uow.Commit();
        }

        private void Cursos(Aluno aluno, IEnumerable<CursoViewModel> cursos)
        {
            foreach (var cursoModel in cursos)
            {
                var curso = _cursoRepository.ObterPorId(cursoModel.Id);

                if(cursoModel.Selecionado && !aluno.Cursos.Contains(curso))
                    aluno.AdicionarCurso(curso);
                
                if(!cursoModel.Selecionado && aluno.Cursos.Contains(curso))
                    aluno.RemoverCurso(curso);
            }
        }


        public void Remover(Guid id)
        {
            var aluno = _alunoRepository.ObterPorId(id);
            if(aluno == null)
                throw new NullReferenceException("Aluno não encontrado.");

            aluno.Inativar();
            _alunoService.Modificar(aluno);
            _uow.Commit();
        }
    }
}