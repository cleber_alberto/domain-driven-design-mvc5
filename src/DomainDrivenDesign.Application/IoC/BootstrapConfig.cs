﻿using DomainDrivenDesign.Application.Applications;
using DomainDrivenDesign.Application.Interfaces;
using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Domain.Interfaces.Services;
using DomainDrivenDesign.Domain.Services;
using DomainDrivenDesign.Infra.Data.DbContexts;
using DomainDrivenDesign.Infra.Data.Interfaces;
using DomainDrivenDesign.Infra.Data.Repositories;
using DomainDrivenDesign.Infra.Data.UoW;
using SimpleInjector;

namespace DomainDrivenDesign.Application.IoC
{
    public class BootstrapConfig
    {
        public static void RegisterServices(Container container)
        {
            container.Register<IDbContext>(() => new DomainDrivenDesignDbContext("DomainDrivenDesignConnectionString"), Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);

            // app
            container.Register<ICursoApplicationService, CursoApplicationService>(Lifestyle.Transient);
            container.Register<IProfessorApplicationService, ProfessorApplicationService>(Lifestyle.Transient);
            container.Register<IAlunoApplicationService, AlunoApplicationService>(Lifestyle.Transient);

            // services
            container.Register<ICursoService, CursoService>(Lifestyle.Transient);
            container.Register<IProfessorService, ProfessorService>(Lifestyle.Transient);
            container.Register<IAlunoService, AlunoService>(Lifestyle.Transient);

            // repositories
            container.Register<ICursoRepository, CursoRepository>(Lifestyle.Scoped);
            container.Register<IProfessorRepository, ProfessorRepository>(Lifestyle.Scoped);
            container.Register<IAlunoRepository, AlunoRepository>(Lifestyle.Scoped);
        }
    }
}