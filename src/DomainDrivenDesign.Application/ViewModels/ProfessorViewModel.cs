﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DomainDrivenDesign.Application.ViewModels
{
    public class ProfessorViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Campo obrigatório.")]
        public string Nome { get; set; }

        [Display(Name = "Sobrenome")]
        [Required(ErrorMessage = "Campo obrigatório.")]
        public string Sobrenome { get; set; }

        [Display(Name = "Curso")]
        [Required(ErrorMessage = "Campo obrigatório.")]
        public Guid CursoId { get; set; }

        public CursoViewModel Curso { get; set; }
    }
}