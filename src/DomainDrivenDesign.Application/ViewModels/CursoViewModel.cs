﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DomainDrivenDesign.Application.ViewModels
{
    public class CursoViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Campo obrigatório.")]
        public string Descricao { get; set; }

        [ScaffoldColumn(false)]
        public bool Selecionado { get; set; }
    }
}