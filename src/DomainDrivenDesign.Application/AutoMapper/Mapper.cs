﻿using System.Collections.Generic;
using AutoMapper;

namespace DomainDrivenDesign.Application.Mapping
{
    public static class Mapper<TClassA, TClassB>
        where TClassA : class
        where TClassB : class
    {
        private static readonly IMapper _mapper;

        static Mapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DomainDrivenDesignProfile>();
            });
            config.AssertConfigurationIsValid();

            _mapper = config.CreateMapper();
        }

        public static TClassB Single(TClassA model)
        {
            return _mapper.Map<TClassA, TClassB>(model);
        }

        public static IEnumerable<TClassB> AsEnumerable(IEnumerable<TClassA> entityList)
        {
            return _mapper.Map<IEnumerable<TClassA>, IEnumerable<TClassB>>(entityList);
        }
    }
}