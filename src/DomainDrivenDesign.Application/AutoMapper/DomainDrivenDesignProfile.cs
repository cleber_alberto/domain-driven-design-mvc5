﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DomainDrivenDesign.Application.ViewModels;
using DomainDrivenDesign.Domain.Entities;

namespace DomainDrivenDesign.Application.Mapping
{
    public class DomainDrivenDesignProfile : Profile
    {
        public DomainDrivenDesignProfile()
        {
            CreateMap<Curso, CursoViewModel>()
                .ForMember(dst => dst.Selecionado, opt => opt.Ignore());

            CreateMap<Professor, ProfessorViewModel>()
                .ForMember(dst => dst.Nome, opt => opt.MapFrom(src => src.Nome.ObterPrimeiroNome()))
                .ForMember(dst => dst.Sobrenome, opt => opt.MapFrom(src => src.Nome.ObterSobrenome()));

            CreateMap<Aluno, AlunoViewModel>()
                .ForMember(dst => dst.Nome, opt => opt.MapFrom(src => src.Nome.ObterPrimeiroNome()))
                .ForMember(dst => dst.Sobrenome, opt => opt.MapFrom(src => src.Nome.ObterSobrenome()))
                .ForMember(dst => dst.Cursos, opt => opt.MapFrom(src => src.Cursos));
        }
    }
}