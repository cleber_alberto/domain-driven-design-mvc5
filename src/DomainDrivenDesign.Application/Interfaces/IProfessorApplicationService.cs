﻿using System;
using System.Collections.Generic;
using DomainDrivenDesign.Application.ViewModels;

namespace DomainDrivenDesign.Application.Interfaces
{
    public interface IProfessorApplicationService
    {
        IEnumerable<ProfessorViewModel> Listar();
        ProfessorViewModel Obter(Guid id);
        void AdicionarOuModificar(ProfessorViewModel model);
        void Remover(Guid id);
    }
}