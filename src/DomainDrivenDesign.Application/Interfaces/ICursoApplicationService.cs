﻿using System;
using System.Collections.Generic;
using DomainDrivenDesign.Application.ViewModels;

namespace DomainDrivenDesign.Application.Interfaces
{
    public interface ICursoApplicationService
    {
        IEnumerable<CursoViewModel> Listar();
        CursoViewModel Obter(Guid id);
        void AdicionarOuModificar(CursoViewModel model);
    }
}