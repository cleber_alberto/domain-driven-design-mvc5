﻿using System;
using System.Collections.Generic;
using DomainDrivenDesign.Application.ViewModels;

namespace DomainDrivenDesign.Application.Interfaces
{
    public interface IAlunoApplicationService
    {
        IEnumerable<AlunoViewModel> Listar();
        AlunoViewModel Obter(Guid id);
        void AdicionarOuModificar(AlunoViewModel model);
        void Remover(Guid id);
    }
}