﻿using DomainDrivenDesign.Application.Applications;
using DomainDrivenDesign.Application.Interfaces;
using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Domain.Interfaces.Services;
using DomainDrivenDesign.Domain.Services;
using DomainDrivenDesign.Infra.Data.DbContexts;
using DomainDrivenDesign.Infra.Data.Interfaces;
using DomainDrivenDesign.Infra.Data.Repositories;
using DomainDrivenDesign.Infra.Data.UoW;
using SimpleInjector;

namespace DomainDrivenDesign.Infra.IoC
{
    public class BootstrapConfig
    {
        public static void RegisterServices(Container container)
        {
            container.Register<IDbContext>(() => new DomainDrivenDesignDbContext("DomainDrivenDesignConnectionString"), Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);

            // app
            container.Register<ICursoApplicationService, CursoApplicationService>();
            container.Register<IProfessorApplicationService, ProfessorApplicationService>();
            container.Register<IAlunoApplicationService, AlunoApplicationService>();

            // services
            container.Register<ICursoService, CursoService>();
            container.Register<IProfessorService, ProfessorService>();
            container.Register<IAlunoService, AlunoService>();

            // repositories
            container.Register<ICursoRepository, CursoRepository>();
            container.Register<IProfessorRepository, ProfessorRepository>();
            container.Register<IAlunoRepository, AlunoRepository>();
        }
    }
}