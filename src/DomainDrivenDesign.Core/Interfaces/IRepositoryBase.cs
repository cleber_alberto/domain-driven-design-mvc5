﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DomainDrivenDesign.Core.Interfaces
{
    public interface IRepositoryBase<TEntity>
    {
        IEnumerable<TEntity> Obter(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "", Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);
        TEntity Encontrar(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "", Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);
        TEntity ObterPorId(object id);
        TEntity ObterPorId(object[] id);
        void Adicionar(TEntity entity);
        void Modificar(TEntity entity);
        void Remover(TEntity entity);
    }
}