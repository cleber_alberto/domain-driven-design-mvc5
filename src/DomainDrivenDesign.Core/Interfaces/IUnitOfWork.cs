﻿using System;

namespace DomainDrivenDesign.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}