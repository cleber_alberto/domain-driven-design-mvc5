﻿namespace DomainDrivenDesign.Core.Interfaces
{
    public interface IServiceBase<in TEntity>
    {
        void Adicionar(TEntity entity);
        void Modificar(TEntity entity);
        void Remover(TEntity entity);
    }
}