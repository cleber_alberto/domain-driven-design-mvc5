﻿using System;

namespace DomainDrivenDesign.Core.Interfaces
{
    public interface IAggregate
    {
        Guid Id { get; } 
    }
}