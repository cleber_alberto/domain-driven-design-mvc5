﻿namespace DomainDrivenDesign.Core.ValueObjects
{
    public class Nome
    {
        public string PrimeiroNome { get; private set; }
        public string Sobrenome { get; private set; }

        protected Nome()
        { }

        public Nome(string primeiroNome, string sobrenome)
        {
            PrimeiroNome = primeiroNome;
            Sobrenome = sobrenome;
        }

        public string ObterPrimeiroNome()
        {
            return this.PrimeiroNome;
        }

        public string ObterSobrenome()
        {
            return this.Sobrenome;
        }

        public string ObterNomeCompleto()
        {
            return $"{this.PrimeiroNome} {this.Sobrenome}";
        }
    }
}