﻿using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Entities;

namespace DomainDrivenDesign.Domain.Interfaces.Repositories
{
    public interface IProfessorRepository : IRepositoryBase<Professor>
    {
         
    }
}