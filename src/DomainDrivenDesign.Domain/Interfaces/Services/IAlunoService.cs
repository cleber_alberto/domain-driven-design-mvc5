﻿using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Entities;

namespace DomainDrivenDesign.Domain.Interfaces.Services
{
    public interface IAlunoService : IServiceBase<Aluno>
    {
         
    }
}