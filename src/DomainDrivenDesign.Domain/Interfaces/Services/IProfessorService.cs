﻿using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Entities;

namespace DomainDrivenDesign.Domain.Interfaces.Services
{
    public interface IProfessorService : IServiceBase<Professor>
    {
         
    }
}