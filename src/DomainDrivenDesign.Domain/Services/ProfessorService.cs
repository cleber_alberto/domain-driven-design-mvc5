﻿using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Domain.Interfaces.Services;

namespace DomainDrivenDesign.Domain.Services
{
    public class ProfessorService : ServiceBase<Professor>, IProfessorService
    {
        public ProfessorService(IProfessorRepository repository) : base(repository)
        {
        }
    }
}