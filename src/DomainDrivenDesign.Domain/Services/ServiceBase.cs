﻿using DomainDrivenDesign.Core.Interfaces;

namespace DomainDrivenDesign.Domain.Services
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity>
    {
        private readonly IRepositoryBase<TEntity> _repository;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public virtual void Adicionar(TEntity entity)
        {
            _repository.Adicionar(entity);
        }

        public virtual void Modificar(TEntity entity)
        {
            _repository.Modificar(entity);
        }

        public virtual void Remover(TEntity entity)
        {
            _repository.Remover(entity);
        }
    }
}