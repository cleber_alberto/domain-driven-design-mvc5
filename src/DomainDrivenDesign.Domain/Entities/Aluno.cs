﻿using System;
using System.Collections.Generic;
using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Core.ValueObjects;

namespace DomainDrivenDesign.Domain.Entities
{
    public class Aluno : IAggregate
    {
        public Guid Id { get; private set; }
        public Nome Nome { get; private set; }
        public bool Ativo { get; private set; }
        public DateTime? DataDeNascimento { get; private set; }

        public virtual ICollection<Curso> Cursos { get; private set; }

        public static Aluno Create(string nome, string sobrenome, DateTime? dataDeNascimento)
        {
            var aluno = new Aluno()
            {
                Id = Guid.NewGuid(),
                Nome = new Nome(nome, sobrenome),
                Ativo = true,
                DataDeNascimento = dataDeNascimento,
                Cursos = new HashSet<Curso>()
            };

            return aluno;
        }

        public void DefinirNome(string nome, string sobrenome)
        {
            this.Nome = new Nome(nome, sobrenome);
        }

        public void DefinirDataDeNascimento(DateTime? dataDeNascimento)
        {
            this.DataDeNascimento = dataDeNascimento;
        }

        public void Inativar()
        {
            this.Ativo = false;
        }

        public void AdicionarCurso(Curso curso)
        {
            this.Cursos.Add(curso);
        }

        public void RemoverCurso(Curso curso)
        {
            this.Cursos.Remove(curso);
        }
    }
}