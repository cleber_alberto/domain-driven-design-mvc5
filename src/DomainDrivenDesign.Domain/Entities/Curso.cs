﻿using System;
using System.Collections.Generic;
using DomainDrivenDesign.Core.Interfaces;

namespace DomainDrivenDesign.Domain.Entities
{
    public class Curso : IAggregate
    {
        public Guid Id { get; private set; }
        public string Descricao { get; private set; }
        public bool Ativo { get; private set; }

        public static Curso Create(string descricao)
        {
            var curso = new Curso()
            {
                Id = Guid.NewGuid(),
                Descricao = descricao,
                Ativo = true
            };

            return curso;
        }

        public void DefinirDescricao(string descricao)
        {
            this.Descricao = descricao;
        }

        public void Inativar()
        {
            this.Ativo = false;
        }
    }
}