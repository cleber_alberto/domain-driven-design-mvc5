﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Xml.Serialization;
using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Core.ValueObjects;

namespace DomainDrivenDesign.Domain.Entities
{
    public class Professor : IAggregate
    {
        public Guid Id { get; private set; }
        public Nome Nome { get; private set; }
        public bool Ativo { get; private set; }
        public Guid CursoId { get; private set; }
        public virtual Curso Curso { get; private set; }

        public static Professor Create(string primeiroNome, string sobrenome, Curso curso)
        {
            var professor = new Professor()
            {
                Id = Guid.NewGuid(),
                Nome = new Nome(primeiroNome, sobrenome),
                Ativo = true,
                CursoId = curso.Id
            };

            return professor;
        }

        public void DefinirNome(string primeiroNome, string sobrenome)
        {
            this.Nome = new Nome(primeiroNome, sobrenome);
        }

        public void DefinirCurso(Curso curso)
        {
            this.CursoId = curso.Id;
        }

        public void Inativar()
        {
            this.Ativo = false;
        }
    }
}