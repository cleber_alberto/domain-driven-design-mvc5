﻿using System;
using System.Data.Entity;
using System.Linq;
using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Infra.Data.Interfaces;

namespace DomainDrivenDesign.Infra.Data.Repositories
{
    public class AlunoRepository : RepositoryBase<Aluno>, IAlunoRepository
    {
        private readonly IDbContext _dbContext;

        public AlunoRepository(IDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
    }
}