﻿using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Infra.Data.Interfaces;

namespace DomainDrivenDesign.Infra.Data.Repositories
{
    public class ProfessorRepository : RepositoryBase<Professor>, IProfessorRepository
    {
        public ProfessorRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}