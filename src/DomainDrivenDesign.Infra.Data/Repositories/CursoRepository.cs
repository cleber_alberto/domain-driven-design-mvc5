﻿using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Domain.Interfaces.Repositories;
using DomainDrivenDesign.Infra.Data.Interfaces;

namespace DomainDrivenDesign.Infra.Data.Repositories
{
    public class CursoRepository : RepositoryBase<Curso>, ICursoRepository
    {
        public CursoRepository(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}