﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainDrivenDesign.Core.Interfaces;
using DomainDrivenDesign.Infra.Data.Interfaces;

namespace DomainDrivenDesign.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity>
        where TEntity : class, IAggregate
    {
        private readonly IDbContext _dbContext;

        public RepositoryBase(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<TEntity> Obter(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "", Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = _dbContext.Set<TEntity>();

            if (filter != null)
                query = query.Where(filter);

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            var result = orderBy != null ? orderBy(query).AsEnumerable() : query.AsEnumerable();
            return result;
        }

        public TEntity Encontrar(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "", Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = _dbContext.Set<TEntity>();

            if (filter != null)
                query = query.Where(filter);

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            var result = orderBy != null ? orderBy(query).AsEnumerable() : query.AsEnumerable();
            return result.FirstOrDefault();
        }

        public TEntity ObterPorId(object id)
        {
            return _dbContext.Set<TEntity>().Find(id);
        }

        public TEntity ObterPorId(object[] id)
        {
            return _dbContext.Set<TEntity>().Find(id);
        }

        public virtual void Adicionar(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Added;
        }

        public virtual void Modificar(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Remover(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Deleted;
        }
    }
}