﻿using System.Data.Entity.ModelConfiguration;
using DomainDrivenDesign.Domain.Entities;

namespace DomainDrivenDesign.Infra.Data.Configurations.EntityConfigurations
{
    public class CursoConfiguration : EntityTypeConfiguration<Curso>
    {
        public CursoConfiguration()
        {
            HasKey(pk => pk.Id);

            Property(p => p.Descricao)
                .IsRequired();
        }
    }
}