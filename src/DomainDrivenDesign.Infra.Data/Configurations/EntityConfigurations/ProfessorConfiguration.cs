﻿using System.Data.Entity.ModelConfiguration;
using DomainDrivenDesign.Domain.Entities;

namespace DomainDrivenDesign.Infra.Data.Configurations.EntityConfigurations
{
    public class ProfessorConfiguration : EntityTypeConfiguration<Professor>
    {
        public ProfessorConfiguration()
        {
            HasKey(pk => pk.Id);

            Property(p => p.Nome.PrimeiroNome)
                .IsRequired();
            Property(p => p.Nome.Sobrenome)
                .IsRequired();
            Property(p => p.CursoId)
                .IsRequired();

            HasRequired(hr => hr.Curso)
                .WithMany()
                .HasForeignKey(fk => fk.CursoId);
        }
    }
}