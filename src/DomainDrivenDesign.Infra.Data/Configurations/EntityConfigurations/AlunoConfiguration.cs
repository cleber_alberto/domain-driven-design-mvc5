﻿using System.Data.Entity.ModelConfiguration;
using DomainDrivenDesign.Domain.Entities;

namespace DomainDrivenDesign.Infra.Data.Configurations.EntityConfigurations
{
    public class AlunoConfiguration : EntityTypeConfiguration<Aluno>
    {
        public AlunoConfiguration()
        {
            HasKey(pk => pk.Id);

            Property(p => p.DataDeNascimento)
                .IsOptional();

            HasMany(hm => hm.Cursos)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("AlunoId");
                    cs.MapRightKey("CursoId");
                });
        }
    }
}