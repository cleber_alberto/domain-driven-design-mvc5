﻿using System.Data.Entity.ModelConfiguration;
using DomainDrivenDesign.Core.ValueObjects;

namespace DomainDrivenDesign.Infra.Data.Configurations.ComplexTypeConfigurations
{
    public class NomeConfiguration : ComplexTypeConfiguration<Nome>
    {
        public NomeConfiguration()
        {
            Property(p => p.PrimeiroNome)
                .HasColumnName("PrimeiroNome");
            Property(p => p.Sobrenome)
                .HasColumnName("Sobrenome");
        }
    }
}