﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DomainDrivenDesign.Domain.Entities;
using DomainDrivenDesign.Infra.Data.Configurations.ComplexTypeConfigurations;
using DomainDrivenDesign.Infra.Data.Configurations.EntityConfigurations;

namespace DomainDrivenDesign.Infra.Data.DbContexts
{
    public class DomainDrivenDesignDbContext : BaseDbContext
    {
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<Professor> Professores { get; set; }
        public DbSet<Aluno> Alunos { get; set; }

        public DomainDrivenDesignDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DomainDrivenDesignDbContext>());
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Remove algumas convenções padrões do EF.
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            // Configuração para os tipos C# x Sql Server.
            modelBuilder.Properties<decimal>()
                .Configure(p => p.HasColumnType("money"));
            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));
            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(60));

            // ComplexType
            modelBuilder.Configurations.Add(new NomeConfiguration());

            // EntityType
            modelBuilder.Configurations.Add(new CursoConfiguration());
            modelBuilder.Configurations.Add(new ProfessorConfiguration());
            modelBuilder.Configurations.Add(new AlunoConfiguration());
        }
    }
}