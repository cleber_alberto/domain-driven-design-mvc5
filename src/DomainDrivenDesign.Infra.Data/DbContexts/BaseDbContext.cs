﻿using System.Data.Entity;
using DomainDrivenDesign.Infra.Data.Interfaces;

namespace DomainDrivenDesign.Infra.Data.DbContexts
{
    public class BaseDbContext : DbContext, IDbContext
    {
        public BaseDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            
        }

        public IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }
    }
}