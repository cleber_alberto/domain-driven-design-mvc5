﻿using System.Data.Entity.Infrastructure;

namespace DomainDrivenDesign.Infra.Data.DbContexts
{
    public class DbContextFactory : IDbContextFactory<DomainDrivenDesignDbContext>
    {
        public DomainDrivenDesignDbContext Create()
        {
            return new DomainDrivenDesignDbContext("DomainDrivenDesignConnectionString");
        }
    }
}